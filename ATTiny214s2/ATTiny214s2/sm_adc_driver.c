//#include "iotn214.h"
#include <avr/io.h>
#include "sm_adc_driver.h"
/*
 * adc_driver.c
 *
 * Created: 2/16/2021 7:55:26 PM
 *  Author: vpandey
 */ 
 int8_t init_adc()
 {
	// Disable digital input buffer
	//PA6_set_isc(PORT_ISC_INPUT_DISABLE_gc);
	// Disable pull-up resistor
	//PA6_set_pull_mode(PORT_PULL_OFF);

	ADC0.CTRLC = ADC_PRESC_DIV8_gc      /* CLK_PER divided by 8 */
	| ADC_REFSEL_VDDREF_gc /* VDD */
	| 0 << ADC_SAMPCAP_bp; /* Sample Capacitance Selection: disabled */

	// ADC0.CTRLD = 0 << ADC_ASDV_bp /* Automatic Sampling Delay Variation: disabled */
	//		 | 0x0 << ADC_SAMPDLY_gp /* Sampling Delay Selection: 0x0 */
	//		 | ADC_INITDLY_DLY0_gc; /* Delay 0 CLK_ADC cycles */

	// ADC0.CTRLE = ADC_WINCM_NONE_gc; /* No Window Comparison */

	// ADC0.DBGCTRL = 0 << ADC_DBGRUN_bp; /* Debug run: disabled */

	// ADC0.EVCTRL = 0 << ADC_STARTEI_bp; /* Start Event Input Enable: disabled */

	ADC0.INTCTRL = 1 << ADC_RESRDY_bp  /* Result Ready Interrupt Enable: enabled */
	| 0 << ADC_WCMP_bp; /* Window Comparator Interrupt Enable: disabled */

	ADC0.MUXPOS = ADC_MUXPOS_AIN11_gc; /* ADC input pin 10 */

	// ADC0.SAMPCTRL = 0x0 << ADC_SAMPLEN_gp; /* Sample length: 0x0 */

	// ADC0.WINHT = 0x0; /* Window Comparator High Threshold: 0x0 */

	// ADC0.WINLT = 0x0; /* Window Comparator Low Threshold: 0x0 */

	ADC0.CTRLA = 1 << ADC_ENABLE_bp     /* ADC Enable: enabled */
	| 1 << ADC_FREERUN_bp  /* ADC Freerun mode: enabled*/
	| ADC_RESSEL_8BIT_gc   /* 8-bit mode */
	| 0 << ADC_RUNSTBY_bp; /* Run standby mode: Disabled */

	return 0;

 }
 /**
 * \brief Enable adc
 * 1. If supported by the clock system, enables the clock to the ADC
 * 2. Enables the ADC module by setting the enable-bit in the ADC control register
 *
 * \return Nothing
 */
void adc_enable()
{
	ADC0.CTRLA |= ADC_ENABLE_bm;
}

/**
 * \brief Disable adc
 * 1. Disables the ADC module by clearing the enable-bit in the ADC control register
 * 2. If supported by the clock system, disables the clock to the ADC
 *
 * \return Nothing
 */
void adc_disable()
{
	ADC0.CTRLA &= ~ADC_ENABLE_bm;
}

/**
 * \brief Start a conversion on adc
 *
 * \param[in] channel The ADC channel to start conversion on
 *
 * \return Nothing
 */
void adc_start_conversion(adc_0_channel_t channel)
{
	ADC0.MUXPOS  = channel;
	ADC0.COMMAND = ADC_STCONV_bm;
}

/**
 * \brief Check if the ADC conversion is done
 *
 * \return The status of ADC converison done check
 * \retval true The ADC conversion is done
 * \retval false The ADC converison is not done
 */
int8_t adc_is_conversion_done()
{
	return (ADC0.INTFLAGS & ADC_RESRDY_bm);
}

/**
 * \brief Read a conversion result from adc
 *
 * \return Conversion result read from the adc ADC module
 */
adc_result_t adc_get_conversion_result(void)
{
	return (ADC0.RES);
}

/**
 * \brief Read the conversion window result from adc
 *
 * \return Returns true when a comparison results in a trigger condition, false otherwise.
 */
int8_t adc_get_window_result(void)
{
	int8_t temp     = (ADC0.INTFLAGS & ADC_WCMP_bm);
	ADC0.INTFLAGS = ADC_WCMP_bm; // Clear intflag if set
	return temp;
}

/**
 * \brief Start a conversion, wait until ready, and return the conversion result
 *
 * \return Conversion result read from the adc ADC module
 */
adc_result_t adc_get_conversion(adc_0_channel_t channel)
{
	adc_result_t res;

	adc_start_conversion(channel);
	while (!adc_is_conversion_done())
		;
	res           = adc_get_conversion_result();
	ADC0.INTFLAGS = ADC_RESRDY_bm;
	return res;
}

/**
 * \brief Return the number of bits in the ADC conversion result
 *
 * \return The number of bits in the ADC conversion result
 */
uint8_t adc_get_resolution()
{
	return (ADC0.CTRLA & ADC_RESSEL_bm) ? 8 : 10;
}
