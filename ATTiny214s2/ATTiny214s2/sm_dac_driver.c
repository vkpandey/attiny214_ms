/*
 * sm_dac_driver.c
 *
 * Created: 2/17/2021 7:36:46 AM
 *  Author: vpandey
 */ 

 #include <avr/io.h>
 //#include <util/delay.h>
 #define LSB_MASK                (0x03)
 void dac_init(void)
 {
	 /* Enable DAC */
	 DAC0.CTRLA = DAC_ENABLE_bm;
 }

 void dac_setVal(uint16_t value)
 {
 //Need to correct byte order
	 /* Store the two LSbs in dac.DATAL */
	 DAC0.DATA = value; //& LSB_MASK) << 6;
	 /* Store the eight MSbs in DAC0.DATAH */
	 //DAC0.DATA = value >> 2;
 }
