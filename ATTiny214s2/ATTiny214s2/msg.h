/*
 * msg.h
 *
 * Created: 2/16/2021 8:33:57 PM
 *  Author: vpandey
 */ 


#ifndef MSG_H_
#define MSG_H_
#include "utils.h"
typedef uint16_t adc_result_t;
#define SRCDEST_ADDRESS   0x20  // just hypothetical, though I can see from Datasheet I2c/SPI	address

#define CMD_ADC_CONVERSION			0x01
#define CMD_DAC_WRITE				0x02
#define ADC_CHANNEL_0				0x01
#define ADC_CHANNEL_1				0x02
#define ADC_CHANNEL_0_N_1			0x03

#define DAC_ACK						0x55
#define DAC_NACK					0xAA

#define ADC_RPLY					0x81
#define DAC_RPLY					0x82
#define RPLY_MIN_PLD_SZ				0x03


#define ADC_DAC_PLD_LN_bp			0x1
#define ADC_DAC_CHNL_bp				0x2


int8_t handle_message(byte* msg);

#endif /* MSG_H_ */