/*
 * utils.c
 *
 * Created: 2/18/2021 8:22:38 AM
 *  Author: Vivek.Pandey
 */ 
#include "utils.h"
int8_t findBitsSet(int8_t num, int8_t *setbitposArr)
{
	int8_t nSetBit = 0;
	for (int8_t i=0; i < 8; i++)
	{
		if(num & (1 << i))
		setbitposArr[nSetBit++] = i + 1;
	}
	return nSetBit;
}