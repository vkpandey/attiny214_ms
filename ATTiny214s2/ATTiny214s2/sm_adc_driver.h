/*
 * adc_driver.h
 *
 * Created: 2/16/2021 8:09:08 PM
 *  Author: vpandey
 */ 
#define RES_10BIT 0x3FF
typedef void (*adc_irq_cb_t)(void);

typedef uint16_t adc_result_t;
//* Analog channel selection */
typedef ADC_MUXPOS_t adc_0_channel_t;

int8_t adc_init();

void adc_enable();

void adc_disable();

/** Function pointer to callback function called by IRQ.
    NULL=default value: No callback function is to be used.
*/
//adc_irq_cb_t adc_window_cb = 0;
