/*
 * utils.h
 *
 * Created: 2/18/2021 8:22:49 AM
 *  Author: Vivek.Pandey
 */ 


#ifndef UTILS_H_
#define UTILS_H_
#include <avr/io.h>
/** Datatype for the result of the ADC conversion */

typedef unsigned char byte;

#define RIGHT_ADJUST(x)		{(x << 8) ? x : ((x << 8) | (x>>8))}
int8_t findBitsSet(int8_t num, int8_t *setbitposArr);
#endif /* UTILS_H_ */