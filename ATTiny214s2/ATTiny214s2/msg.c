/*
 * msg.c
 *
 * Created: 2/18/2021 10:07:20 AM
 *  Author: Vivek.Pandey
 */ 
#include "msg.h"
#include "utils.h"
#include <stdint.h>
#if RUNNING_UTs
	byte output[20] = {0};
#endif

void send_msg(void * dest, byte *p)
{
	// Code need to be written, depending on how we are sending (I2C, SPI, ..)
	if(dest == 0)
	{
	#if RUNNING_UTs
		memcpy(output, p, 2); // wrote 2 for testing hardcoded
	#endif
	}
}

void recv_msg(void *src, byte *p)
{
	// Code need to be written, depending on how we are sending (I2C, SPI, ..)
}
int8_t(*findChannels)(int8_t, int8_t *) = &findBitsSet;
int8_t handle_message(byte* msg)
{
	if(msg == 0)
	return 1;
	byte cmd = 	msg[0];
	//msg++;
	byte ln = msg[ADC_DAC_PLD_LN_bp];
	int8_t channelArr[8];
	int8_t nchannel = findChannels(msg[ADC_DAC_CHNL_bp], channelArr);
	byte channel = channelArr[nchannel-1]; // since we are assuming only one channel 0 so I am making that only 1 channel come, in future we can expand it to get multiple
	//parse the message
	switch (cmd)
	{
		case CMD_ADC_CONVERSION:
		{
			adc_result_t res = adc_get_conversion(channel);
			byte bRAdjusted[] = RIGHT_ADJUST(res);
			byte resp_msg[5] = {ADC_RPLY, RPLY_MIN_PLD_SZ,ADC_CHANNEL_0, bRAdjusted[0],bRAdjusted[1]};
			// Send the message to requester
			send_msg((void *)SRCDEST_ADDRESS, resp_msg);
		}
		break;
		case CMD_DAC_WRITE:
		{
			int16_t valToWrt = *((int16_t*)&msg[ADC_DAC_CHNL_bp+1]);
			byte *bRAdjusted = RIGHT_ADJUST(valToWrt);
			byte resp_msg[] = {DAC_RPLY, RPLY_MIN_PLD_SZ,ADC_CHANNEL_0, DAC_ACK};
			dac_setVal(*(uint16_t*)bRAdjusted);
			send_msg((void *)SRCDEST_ADDRESS, resp_msg);
		}
		break;

	}
	
	

	return 0;
}