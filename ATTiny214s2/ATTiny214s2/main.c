/*
 * main.c
 *
 * Created: 2/16/2021 7:53:28 PM
 *  Author: vpandey
 */ 

#include <xc.h>
#include "msg.h"
#include "sm_adc_driver.h"
#include "sm_dac_driver.h"


//int8_t handle_message(unsigned char* msg);

//

int main(void)
{
	 uint16_t dacVal = 0;
	 volatile uint16_t adcVal = 0;
	 unsigned char* msg = 0;
	dac_init();
	init_adc();
    while(1)
    {
        //TODO:: Please write your application code 
		recv_msg((void *)SRCDEST_ADDRESS, msg);
		handle_message(msg);
    }
}


