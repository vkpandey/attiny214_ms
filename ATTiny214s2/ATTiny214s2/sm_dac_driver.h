/*
 * sm_dac_driver.h
 *
 * Created: 2/17/2021 8:04:08 AM
 *  Author: vpandey
 */ 


#ifndef SM_DAC_DRIVER_H_
#define SM_DAC_DRIVER_H_

void dac_init(void);
void dac_setVal(uint16_t value);


#endif /* SM_DAC_DRIVER_H_ */